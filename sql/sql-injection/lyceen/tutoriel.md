## Qu'est-ce que le SQL ?

Le langage SQL correspond au langage des bases de données ! Celui-ci se présente sous différentes requêtes, la plus connues d'entre elles étant :

`SELECT * FROM utilisateurs WHERE pseudonyme='?' AND mot_de_passe='?'`

Cette requête peut être associée au tableau `utilisateurs` qui ressemblerait à cela  :

| pseudonyme | mot_de_passe | est_administrateur |
|------------|--------------|--------------------|
|      admin |       test11 |                oui |
|       jean |         sjdl |                non |

Les mots clés en majuscule sont les commandes :
`SELECT` = permet de choisir les colonnes que l'on veut voir, `*` veut dire toutes les colonnes.
`FROM` = permet de choisir la table que l'on veut voir, ici celle juste au dessus.
`WHERE` = permet de filtrer les réponses.
`AND` = permet d'avoir plusieurs conditions remplies en même temps.
`OR` = permet d'avoir une condition OU une autre de remplie.

Si l'utilisateur remplit un formulaire avec pour nom "jean" et mot de passe "oui", on aura la requête suivante :

`SELECT * FROM utilisateurs WHERE pseudonyme='jean' AND mot_de_passe='oui'`

### Cas d'usage :

Ce genre de requête apparaît notamment lorsque l'on remplis des formulaires sur Internet !

## Qu'est-ce qu'une injection SQL ?

Une injection SQL est une faille de sécurité qui permet aux attaquants d'interférer avec les requêtes de base de données d'une application. Cette vulnérabilité peut permettre aux attaquants de visualiser, modifier ou supprimer des données auxquelles ils ne devraient pas accéder, y compris les informations d'autres utilisateurs ou toutes les données auxquelles l'application peut accéder. De telles actions peuvent entraîner des modifications permanentes de la fonctionnalité ou du contenu de l'application, voire compromettre le serveur ou provoquer un déni de service.

Si l'on reprend l'exemple précédent, je remplis le formulaire en mettant "admin" comme nom et je remplis le mot de passe comme étant `'`, la requête devient : 

`SELECT * FROM utilisateurs WHERE pseudonyme='admin' AND mot_de_passe='''`

Et là plus rien ne fonctionne ! Le serveur a besoin que le paramètre soit entre des guillemets mais il en compte 3 !!

Avec ma nouvelle connaissance de l'erreur que le serveur a commise je me décide à entrer `' or ''='` comme mot de passe et on obtient : 

`SELECT * FROM utilisateurs WHERE pseudonyme='admin' AND mot_de_passe='' OR ''=''`

Le serveur va donc comprendre "si le mot de passe est vide ou si 'rien' vaut 'rien'", la deuxieme condition étant vraie, je suis désormais connecté !!!